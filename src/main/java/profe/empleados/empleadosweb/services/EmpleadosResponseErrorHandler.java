package profe.empleados.empleadosweb.services;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import profe.empleados.exceptions.EmpleadoNoEncontradoException;
import profe.empleados.exceptions.EmpleadoYaExisteException;
import profe.empleados.exceptions.EmpleadosOpNoAutorizadaException;

public class EmpleadosResponseErrorHandler extends DefaultResponseErrorHandler {

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		switch (response.getStatusCode()) {
		
		case NOT_FOUND:
			throw new EmpleadoNoEncontradoException();
			
		case CONFLICT:
			throw new EmpleadoYaExisteException();
			
		case UNAUTHORIZED:
		case FORBIDDEN:
			throw new EmpleadosOpNoAutorizadaException();
			
		default:
			super.handleError(response);
		}
		
	}

	
}
