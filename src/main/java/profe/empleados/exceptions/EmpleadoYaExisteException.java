package profe.empleados.exceptions;

public class EmpleadoYaExisteException extends EmpleadosException {

	public EmpleadoYaExisteException() {
		// TODO Auto-generated constructor stub
	}

	public EmpleadoYaExisteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoYaExisteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoYaExisteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoYaExisteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
